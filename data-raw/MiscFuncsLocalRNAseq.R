## Functions used to make spaces
## by esfahani@aices.rwth-aachen.de

GroupSpaceMakerRNAseq <- function(PRJ, Annot, Method = "PhysioScore", LinearOrRNASeq = "RNASeq"){
  SimpletonIndices <- which(paste0(Annot$Curated_Control[-nrow(Annot)],
                                   Annot$Curated_Control[-1]) == "TC")
  if(length(SimpletonIndices)==0){
    return(SimpletonSpaceMakerRNAseq(PRJ, Annot, Method, LinearOrRNASeq))
  } else {
    Indx <- 1
    while(Indx < max(SimpletonIndices)){
      SubAnnotIndices <- Indx:min(SimpletonIndices[Indx<SimpletonIndices])
      if(length(SubAnnotIndices)<2) stop("Problem in indexing")
      AnnotSub <- Annot[SubAnnotIndices,]
      if(exists("SpaceMat")){
        SpaceMat <- cbind(SpaceMat,SimpletonSpaceMakerRNAseq(PRJ, AnnotSub, Method, LinearOrRNASeq))
      } else {
        SpaceMat <- SimpletonSpaceMakerRNAseq(PRJ, AnnotSub, Method, LinearOrRNASeq)
      }
      Indx <- max(SubAnnotIndices) + 1
    }
    SubAnnotIndices <- Indx:nrow(Annot)
    if(length(SubAnnotIndices)<2) stop("Problem in indexing")
    AnnotSub <- Annot[SubAnnotIndices,]
    SpaceMat <- cbind(SpaceMat,SimpletonSpaceMakerRNAseq(PRJ, AnnotSub, Method, LinearOrRNASeq))
    return(SpaceMat)
  }
}

SimpletonSpaceMakerRNAseq <- function(PRJ, Annot, Method, LinearOrRNASeq){
  message("Making a simpleton space from ", PRJ)
  GSEexpr <- readRDS(paste0("data-raw/ExprSets/",PRJ,".rds"))

  SampleIDS <- colnames(GSEexpr)

  MatchIndx <- match(Annot$ID,SampleIDS, nomatch = 0)
  if(any(MatchIndx==0)) stop("Sample couldn't be found in PRJ Object")

  GSEexpr <- GSEexpr[,MatchIndx]

  if(Annot$Curated_Control[1]!="C") stop("expecting to have control at first")
  if(max(which(Annot$Curated_Control=="C")) >=
     min(which(Annot$Curated_Control=="T")))
    stop("expecting to have a cluster of control samples at the start,",
         "followed by cluster(s) of experiment replicates")

  colnames(GSEexpr) <- make.names(paste(Annot$Curated_PRJ, Annot$Curated_GSE_,
                                        Annot$Curated_group,
                                        Annot$Curated_Treatment, Annot$Curated_Time,
                                        Annot$Curated_Stress, sep = ".."))

  return(spaceMaker(GeneExMatrix = GSEexpr, Output = Method,
                    LinearOrRNASeq = LinearOrRNASeq))
}
