## Functions used to make spaces
## by esfahani@aices.rwth-aachen.de

GroupSpaceMaker <- function(GSE, Annot, Method = "PhysioScore", LinearOrRNASeq = "Linear"){
  SimpletonIndices <- which(paste0(Annot$Curated_Control[-nrow(Annot)],
                                   Annot$Curated_Control[-1]) == "TC")
  if(length(SimpletonIndices)==0){
    return(SimpletonSpaceMaker(GSE, Annot, Method, LinearOrRNASeq))
  } else {
    Indx <- 1
    while(Indx < max(SimpletonIndices)){
      SubAnnotIndices <- Indx:min(SimpletonIndices[Indx<SimpletonIndices])
      if(length(SubAnnotIndices)<2) stop("Problem in indexing")
      AnnotSub <- Annot[SubAnnotIndices,]
      if(exists("SpaceMat")){
        SpaceMat <- cbind(SpaceMat,SimpletonSpaceMaker(GSE, AnnotSub, Method, LinearOrRNASeq))
      } else {
        SpaceMat <- SimpletonSpaceMaker(GSE, AnnotSub, Method, LinearOrRNASeq)
      }
      Indx <- max(SubAnnotIndices) + 1
    }
    SubAnnotIndices <- Indx:nrow(Annot)
    if(length(SubAnnotIndices)<2) stop("Problem in indexing")
    AnnotSub <- Annot[SubAnnotIndices,]
    SpaceMat <- cbind(SpaceMat,SimpletonSpaceMaker(GSE, AnnotSub, Method, LinearOrRNASeq))
    return(SpaceMat)
  }
}

SimpletonSpaceMaker <- function(GSE, Annot, Method, LinearOrRNASeq){

  if(is.character(GSE)){
    message("Making a simpleton space from ", GSE)
    GSEexpr <- readRDS(paste0("data-raw/ExprSets/",GSE,".rds"))
  } else {
    GSEexpr <- GSE
  }

  if(max(GSEexpr, na.rm = T) > 100) stop("Numbers not in log-scale?")
  if(max(GSEexpr, na.rm = T) <= 0) stop("Numbers not in log-scale?")

  GSEpDGSMs <- colnames(GSEexpr)

  MatchIndx <- match(Annot$gsm,GSEpDGSMs, nomatch = 0)
  if(any(MatchIndx==0)) stop("Sample couldn't be found in GSE Object")

  GSEexpr <- GSEexpr[,MatchIndx]

  if(Annot$Curated_Control[1]!="C") stop("expecting to have control at first")
  if(max(which(Annot$Curated_Control=="C")) >=
     min(which(Annot$Curated_Control=="T")))
    stop("expecting to have a cluster of control samples at the start,",
         "followed by cluster(s) of experiment replicates")

  colnames(GSEexpr) <- make.names(paste(Annot$Curated_GSE_, Annot$Curated_group,
                                        Annot$Curated_Treatment, Annot$Curated_Time,
                                        Annot$Curated_Stress, sep = ".."))

  library(PhysioSpaceMethods)
  return(spaceMaker(GeneExMatrix = GSEexpr, Output = Method,
                    LinearOrRNASeq = LinearOrRNASeq))
}
