## Preparing and saving Arabidopsis thaliana space (from scratch):
## based on local GSEs that are normalized together
# By esfahani@aices.rwth-aachen.de


library(PhysioSpaceMethods)
library(GEOquery)
library(progress)
source("data-raw/MiscFuncsLocal.R")

AllRawAnnots <- read.csv("data-raw/Arabidopsis/phenoAnnotation__ATH1-121501_ordered.csv")

AllRawGSEs <- levels(AllRawAnnots$Curated_GSE_)


#Main Calc:
tmpList <- list()
PB <- progress_bar$new(format = "(:spin) [:bar] :percent eta: :eta",
                       total = length(AllRawGSEs),
                       clear = FALSE)
for(gse in AllRawGSEs){
  SaidAnnot <- AllRawAnnots[AllRawAnnots$Curated_GSE_==gse,]
  tmpList[[gse]] <- GroupSpaceMaker(GSE = gse, Annot = SaidAnnot, Method = "FoldChange")
  PB$tick()
}

for(K in seq_along(tmpList)){
  if(K==1){
    AT_Stress_Space_Detailed <- tmpList[[K]]
  } else {
    while(all(rownames(AT_Stress_Space_Detailed)!=rownames(tmpList[[K]]))){
      tmpMatching <- match(rownames(AT_Stress_Space_Detailed),
                           rownames(tmpList[[K]]))
      if(anyNA(tmpMatching)) stop("There's a big problem here!")
      tmpList[[K]] <- tmpList[[K]][tmpMatching,]
    }
    AT_Stress_Space_Detailed <- cbind(AT_Stress_Space_Detailed, tmpList[[K]])
  }
}

##Removing the axes with "not enough" high scores (which means not enough diff. exp. genes, a.k.a. noisy):
crappyAxIndx <- apply(AT_Stress_Space_Detailed,2,function(x) length(unique(x))) < nrow(AT_Stress_Space_Detailed)/10
AT_Stress_Space_Detailed <- AT_Stress_Space_Detailed[,!crappyAxIndx]

##Convert Affy IDs to Entrez IDs:
ATH1ChipInfo <- read.delim(file = "data-raw/Arabidopsis/AT-raw/ArabidopsisATH1GenomeArray.txt",
                           header = T, skip=18)
rownames(AT_Stress_Space_Detailed) <- ATH1ChipInfo$Entrez.Gene[match(x = rownames(AT_Stress_Space_Detailed),
                                                          table = ATH1ChipInfo$Probe.Set.ID)]
AT_Stress_Space_Detailed <- AT_Stress_Space_Detailed[rownames(AT_Stress_Space_Detailed)!="---",] #Lost 1196 rows in conversion
#take care of multiple-matched IDs:
TKOMMIDs <- function(x){
  NAMESplitted <- strsplit(rownames(x), split = " /// ")[[1]]
  if(length(NAMESplitted) > 1){
    return(matrix(data = rep(x,length(NAMESplitted)), nrow = length(NAMESplitted),
                  byrow = TRUE, dimnames = list(unlist(NAMESplitted), colnames(x))))
  } else {
    return(x)
  }
}

K <- 2 #I'm cheating a bit cause I know the first and last rows don't have duplicates: rownames(AT_Stress_Space_Detailed)[c(1,21614)]
while(K < nrow(AT_Stress_Space_Detailed)){
  if(length(strsplit(rownames(AT_Stress_Space_Detailed)[K], split = " /// ")[[1]]) > 1){
    beforeMat <- AT_Stress_Space_Detailed[1:(K-1),,drop=FALSE]
    afterMat <- AT_Stress_Space_Detailed[(K+1):nrow(AT_Stress_Space_Detailed),,drop=FALSE]
    AT_Stress_Space_Detailed <- rbind(beforeMat, TKOMMIDs(AT_Stress_Space_Detailed[K,,drop=FALSE]), afterMat)
  }
  K <- K + 1
  print(K)
}


##Generalization Annotation:
ColTmp <- colnames(AT_Stress_Space_Detailed)
names(ColTmp) <- sapply(strsplit(ColTmp,split = "..", fixed = T),
                        function(x) x[length(x)])
names(ColTmp)[names(ColTmp)=="Maturity"] <- "Unstressed"
names(ColTmp)[ColTmp == "GSE58616..light..light.hs..0..Light"] <- "Heat"
names(ColTmp)[ColTmp == "GSE5539..Columbia..MBF1c..0..Heat"] <- "Mutant"
names(ColTmp)[ColTmp == "GSE22671..Arabidopsis..Dark..0..Light"] <- "Dark"
colnames(AT_Stress_Space_Detailed) <- ColTmp

names(colnames(AT_Stress_Space_Detailed))[names(colnames(AT_Stress_Space_Detailed))=="Transgenic"] <- "Mutant"


##Removing the "non-stress" axes:
#There were some samples (eventual axes) that are not actual traditional stress
#(Stress here means any inhibition on normal processes of plant life), e.g.
#"Dark", "CO2" and "Unstressed":
tmpNames <- colnames(AT_Stress_Space_Detailed)
AT_Stress_Space_Detailed <- AT_Stress_Space_Detailed[,names(colnames(AT_Stress_Space_Detailed)) != "CO2" &
                                     names(colnames(AT_Stress_Space_Detailed)) != "Dark" &
                                     names(colnames(AT_Stress_Space_Detailed)) != "Unstressed"]
#names of colnames was lost, will reassign it:
colnames(AT_Stress_Space_Detailed) <- tmpNames[names(tmpNames) != "CO2" &
                                                 names(tmpNames) != "Dark" &
                                                 names(tmpNames) != "Unstressed"]

#I also remove "Aluminum" cause it's only one axis, and the matching quality is
#crappy:
tmpNames <- names(colnames(AT_Stress_Space_Detailed))
AT_Stress_Space_Detailed <- AT_Stress_Space_Detailed[,
                                names(colnames(AT_Stress_Space_Detailed)) !=
                                  "Aluminum"]
names(colnames(AT_Stress_Space_Detailed)) <-
  tmpNames[tmpNames!="Aluminum"]


# saveRDS(object = AT_Stress_Space_Detailed, file = "./data-raw/Arabidopsis/AT_Stress_Space_Detailed.rds")
usethis::use_data(AT_Stress_Space_Detailed)



##Making the brief version:
StresGrups <- names(colnames(AT_Stress_Space_Detailed))
AT_Stress_Space <- matrix(NA, nrow = nrow(AT_Stress_Space_Detailed),
                               ncol = length(unique(StresGrups)),
                               dimnames = list(rownames(AT_Stress_Space_Detailed),
                                               unique(StresGrups)))
for(StreS in colnames(AT_Stress_Space)){
  AT_Stress_Space[,StreS] <- apply(AT_Stress_Space_Detailed[,StreS==StresGrups,drop=FALSE],
                                    1, mean)
}

##Removing "Mutant" (because having one big group of all Mutants together doesn't make sense!):
AT_Stress_Space <- AT_Stress_Space[,colnames(AT_Stress_Space)!="Mutant"]

# saveRDS(object = AT_Stress_Space, file = "./data-raw/Arabidopsis/AT_Stress_Space.rds")
usethis::use_data(AT_Stress_Space)

##Making the Meta-Spaces (based on meta groups found on the paper 'under preparation',
#email esfahani@aices.rwth-aachen.de to get access to a draft/link):
names(ConvTable) <- ConvTable <- unique(names(colnames(AT_Stress_Space_Detailed)))
ConvTable[c("Biotic","Hormone","Biotic.Hormone")] <- "BioMone"
ConvTable[c("Drought","Salt","Osmotic")] <- "DrouSaTic"
ConvTable[c("Light","UV")] <- "LighUV"

AT_Stress_Space_Detailed_Meta <- AT_Stress_Space_Detailed
names(colnames(AT_Stress_Space_Detailed_Meta)) <- ConvTable[names(colnames(AT_Stress_Space_Detailed))]

StresGrupsMeta <- names(colnames(AT_Stress_Space_Detailed_Meta))
AT_Stress_Space_Meta <- matrix(NA, nrow = nrow(AT_Stress_Space_Detailed_Meta),
                               ncol = length(unique(StresGrupsMeta)),
                               dimnames = list(rownames(AT_Stress_Space_Detailed_Meta),
                                               unique(StresGrupsMeta)))
for(StreS in colnames(AT_Stress_Space_Meta)){
  AT_Stress_Space_Meta[,StreS] <- apply(AT_Stress_Space_Detailed_Meta[,StreS==StresGrupsMeta,drop=FALSE],
                                        1, mean)
}

##Removing "Mutant" (because having one big group of all Mutants together doesn't make sense!):
AT_Stress_Space_Meta <- AT_Stress_Space_Meta[,colnames(AT_Stress_Space_Meta)!="Mutant"]

# saveRDS(object = AT_Stress_Space_Meta, file = "./data-raw/Arabidopsis/AT_Stress_Space_Meta.rds")
usethis::use_data(AT_Stress_Space_Meta)


##Making self-scores (ran this on CLUSTER cause it could take a while):
# AT_Self <- calculatePhysioMap(InputData = AT_Stress_Space_Detailed,
#                               Space = AT_Stress_Space_Detailed,
#                               GenesRatio = 0.03, NumbrOfCores = 48)
# saveRDS(object = AT_Self, file = "./data-raw/Arabidopsis/AT_Self.rds")

